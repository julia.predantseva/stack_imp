import MyArray from '../index';

describe('tests for method sort', () => {
  test('instance should not have Own Property sort', () => {
    const arr = new MyArray(1, 4, 0);
    expect(Object.prototype.hasOwnProperty.call(arr, 'sort')).toBeFalsy();
  });
  test('instance should have method sort', () => {
    const arr = new MyArray(1, 4, 0);
    expect(arr.sort).toBeInstanceOf(Function);
  });
  test('should mutate instance', () => {
    const a = new MyArray(1, 4, 0);
    a.sort();
    expect(a).toEqual(new MyArray(0, 1, 4));
  });
  test('Infinity and NaN should be after numbers and before strings if method called without arguments', () => {
    const arr = new MyArray('b', 1, 2, 25, NaN, Infinity, 'a');
    arr.sort();
    expect(arr).toEqual(new MyArray(1, 2, 25, Infinity, NaN, 'a', 'b'));
  });
  test('should sort as strings if method called without arguments', () => {
    const arr = new MyArray(1, 2, 25, 12, 45, 31, 23);
    expect(arr.sort()).toEqual(new MyArray(1, 12, 2, 23, 25, 31, 45));
  });
  test('All empty elements should be sorted to the end of the array', () => {
    const a = new MyArray(5, 4, 3, 2, 1);
    delete a[1];
    a.sort();
    expect(a[4] in a).toBeFalsy();
  });
  test('All undefined elements should be sorted to the end of the array, with no call to "compareFunction"', () => {
    const a = new MyArray(5, 4, undefined, null, 1);
    delete a[1];
    const mockCB = jest.fn();
    a.sort(mockCB);
    expect(a).toHaveLength(5);
    expect(a[3]).toBeUndefined();
    expect(a[4]).toBeUndefined();
    expect(mockCB.mock.calls[0]).not.toContain(undefined);
    expect(mockCB.mock.calls[1]).not.toContain(undefined);
  });
  test('should take 1 required argument', () => {
    const arr = new MyArray(1, 4, 0);
    expect(arr.sort).toHaveLength(1);
  });
});
