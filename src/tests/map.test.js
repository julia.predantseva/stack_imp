import MyArray from '../index';

describe('tests for map()', () => {
  test('instance does not have own property map', () => {
    const arr = new MyArray();
    expect(Object.prototype.hasOwnProperty.call(arr, 'map')).toBeFalsy();
  });

  test('instance has method map', () => {
    const arr = new MyArray();
    expect(arr.map).toBeInstanceOf(Function);
  });

  test('callback should have 3 arguments (value, index, array)', () => {
    const arr = new MyArray(1, 2, 5, 5, 5);
    const mockCallback = jest.fn();
    arr.map(mockCallback);
    expect(mockCallback).toBeCalledWith(1, 0, arr);
  });

  test('method should execute a callback function on each element of the array', () => {
    const arr = new MyArray(
      1,
      2,
      5,
      NaN,
      Infinity,
      undefined,
      null,
      Object(),
      function() {
        return true;
      }
    );
    arr.push(7);
    const mockCallback = jest.fn();
    arr.map(mockCallback);
    expect(mockCallback).toHaveBeenCalledTimes(10);
  });

  test('a new array should have the same length', () => {
    const arr = new MyArray(1, 2, 5);
    const mockCallback = jest.fn();
    const newArr = arr.map(mockCallback);
    expect(newArr).toHaveLength(3);
  });

  test('method should have minimum one argument', () => {
    const arr = new MyArray(1, 2, 3);
    expect(arr.map).toHaveLength(1);
  });

  test('callback function should use "optional this", if it is provided as a second argument', () => {
    const arr = new MyArray(1, 2, 5);
    const mockCallback = jest.fn();
    const optionalThis = { a: 10 };
    arr.map(mockCallback.mockReturnThis(), optionalThis);
    expect(mockCallback).toHaveReturnedWith(optionalThis);
  });
});
