import MyArray from '../index';

describe('tests for instance', () => {
  test('instance does not have any own property except length', () => {
    const arr = new MyArray();
    expect(arr).toHaveLength(0);
    expect(Object.prototype.hasOwnProperty.call(arr, 'length')).toBeTruthy();
  });

  test.todo(
    'should throw an error if set length not a positive integer and bigger then 2^32'
  );

  test.todo('should set length converted to positive int values ');

  test.todo('adding new elements should increase length of instance');

  test('prototype have only declared methods and constructor', () => {
    const arr = new MyArray();
    const obj = {
      pop: {},
      push: {},
      shift: {},
      unshift: {},
      filter: {},
      map: {},
      forEach: {},
      reduce: {},
      toString: {},
      sort: {},
      constructor: {}
    };
    expect(Object.getPrototypeOf(arr)).toMatchObject(obj);
  });
});
